#!/bin/bash

#load ssh identities
ssh-add -l

cd /code

if [ -f "composer.json" ]; then
    composer install
fi

#nette kdyby/doctrine using
#php www/index.php orm:schema-tool:create