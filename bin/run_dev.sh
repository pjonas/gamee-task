#!/usr/bin/env bash
# docker-compose wrapper 
# substitute variables in docker-compose.yml

export TARGET_UID=`id -u`
export TARGET_GID=`id -g`
export RUN_DIR=$(dirname "$SCRIPT_FILE")

SCRIPT_FILE=$(python3 -c "import os; print(os.path.realpath('$0'))")
SCRIPT_DIR=$(dirname "$SCRIPT_FILE")
APP_DIR="$SCRIPT_DIR"

cat "$APP_DIR/../.env/dev/docker-compose.yml" | envsubst '$PWD:$TARGET_GID:$TARGET_UID:$HOME:$RUN_DIR' | docker-compose -f - "$@"