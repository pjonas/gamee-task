<?php

namespace Model;

class Redis
{
	/** @var  \Redis */
	protected $redis;
	
	/**
	 * Redis constructor.
	 *
	 * @param string $url url for connect into redis database
	 * @param int $port port of running database
	 * @param int $dbIndex index of Redis database
	 */
	public function __construct(string $url, int $port, int $dbIndex)
	{
		$redis = new \Redis();
		$redis->connect($url, $port);
		$redis->select($dbIndex);
		
		$this->redis = $redis;
	}
	
	/**
	 * Return value from actual selected redis database
	 *
	 * @param string $key
	 * @return bool|string
	 */
	public function get($key){
		return $this->redis->get($key);
	}
	
	/**
	 * Return first 10 items in list
	 *
	 * @param int $gameId
	 * @return array
	 *
	 * @todo: when is on 10 place more than one record, is correct get all ? now it get only 10 rows.
	 */
	public function getTop(int $gameId) : array
	{
		$da = json_decode($this->get('game_' . $gameId));
		$result = [];;
		if(is_null($da))
		{
			$da = [];
		}
		
		foreach ($da as $row)
		{
			if(!isset($result[$row->player]))
			{
				$result[$row->player] = new Player($row->player, $row->score);
			}
			$result[$row->player]->addScore($row->score);
		}
		
		usort($result, array("Model\Player", "compare"));
		
		return array_slice($result,0,10);
	}
	
	/**
	 * Add new score item into redis cache
	 *
	 * @param int $gameId
	 * @param int $playerId
	 * @param int $score
	 * @return bool
	 */
	public function addNewScore(int $gameId,int $playerId,int $score) : bool
	{
		try{
			$actualData = json_decode($this->get('game_' . $gameId));
			
			$actualData[] = [
				'player' => $playerId,
				'score' => $score
			];
			
			$this->redis->set('game_'.$gameId, json_encode($actualData));
			return true;
			
		}catch(\Exception $e)
		{
			return false;
		}
	}
}