<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 17.10.17
 * Time: 18:22
 */

namespace Model;

use Nette\SmartObject;

/**
 * Class Player
 *
 * @package Model
 *
 * @todo: now are variables public, after optimizing  change to private.
 */
class Player
{
	use SmartObject;
	
	/** @var int */
	public $id;
	
	/** @var  int */
	public $score;
	
	/**
	 * Player constructor.
	 *
	 * @param int $id ID of player
	 * @param int $score Score
	 */
	public function __construct(int $id,int $score)
	{
		$this->id = $id;
		$this->score = $score;
	}
	
	/**
	 * @return mixed
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @param mixed $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}
	
	/**
	 * @return mixed
	 */
	public function getScore(): int
	{
		return $this->score;
	}
	
	public function addScore(int $score)
	{
		$this->score += $score;
	}
	
	/**
	 * @param mixed $score
	 */
	public function setScore(int $score)
	{
		$this->score = $score;
	}
	
	/**
	 * comparing function
	 *
	 * @param $a
	 * @param $b
	 * @return int
	 *
	 * @todo: isn't better pointers?
	 */
	function compare(Player $a, Player $b)
	{
		if ($a->getScore() == $b->getScore()) {
			return 0;
		}
		return ($a->getScore() < $b->getScore()) ? +1 : -1;
	}
}