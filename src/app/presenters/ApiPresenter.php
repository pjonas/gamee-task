<?php

namespace Presenters;

use Model\Redis;
use Nette\Application\UI\Presenter;
use Nette\Application\Responses\JsonResponse;

class ApiPresenter extends Presenter
{
	/** @var  Redis @inject */
	public $model;
	
	public function renderDefault()
	{
		$this->redirect('Homepage:default');
	}
	
	public function actionNewScore()
	{
		$data = $this->getResponseBase();
		$header = $this->getRequest()->getPost();
		
		if(isset($header["gameId"]) && isset($header["player"]) && isset($header["score"])){
			$res = 	$this->model->addNewScore($header["gameId"], $header["player"], $header["score"]);
			$this->sendResponse(new JsonResponse($data, "application/json;charset=utf-8" ));
		}
		
		$data['error'] = [
			'code' => 32600,
			'message' => 'invalid request',
		];
		$data['id'] = $header["gameId"];
		
		$this->sendResponse(new JsonResponse($data, "application/json;charset=utf-8" ));
	}
	
	public function actionTopScore($id)
	{
		$response = $this->getResponseBase();
		$response['data'] = $this->model->getTop((int)$id);
		
		$this->sendResponse(new JsonResponse($response, "application/json;charset=utf-8" ));
	}
	
	/**
	 * Get base json array object
	 * @return array
	 */
	protected function getResponseBase()
	{
		$data = [];
		$data['jsonrpc'] = '2.0';
		$data['result'] = 'success';
		return $data;
	}
}
